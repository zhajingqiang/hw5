import sys
import zmq
import time
import re
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
import numpy as np
import pyaudio
import wave
import os

class anim():
	def __init__(self,topic = 42, port1 = 5558, port2 = 5557):
		self.port1 = port1
		self.port2 = port2
		self.topic = topic
		self.wf1 = wave.open('laugh.wav', 'rb')
		self.wf2 = wave.open('fanfare.wav', 'rb')
		self.p = pyaudio.PyAudio()
		self.CHUNK1 = 1024
		self.CHUNK2 = 10
		self.stream1 = self.p.open(format=self.p.get_format_from_width(self.wf1.getsampwidth()),
                channels=self.wf1.getnchannels(),
                rate=self.wf1.getframerate(),
                output=True)
		self.stream2 = self.p.open(format=self.p.get_format_from_width(self.wf2.getsampwidth()),
                channels=self.wf2.getnchannels(),
                rate=self.wf2.getframerate(),
                output=True)
		self.data1 = self.wf1.readframes(self.CHUNK1)
		self.data2 = self.wf2.readframes(self.CHUNK2)

		context = zmq.Context()
		self.socket1 = context.socket(zmq.SUB)
		self.socket1.connect ("tcp://localhost:%s" % self.port1)
		self.socket1.setsockopt(zmq.SUBSCRIBE, "%d"% self.topic)
		self.socket2 = context.socket(zmq.SUB)
		self.socket2.connect ("tcp://localhost:%s" % self.port2)
		self.socket2.setsockopt(zmq.SUBSCRIBE, "%d"% self.topic)
		self.myfig,self.ax= plt.subplots()
		ani = animation.FuncAnimation(self.myfig,self.update, interval=10,blit=False)
		# ani.save('broadcaster.mp4')	
		plt.show()	
		self.stream1.stop_stream()
		self.stream1.close()
		self.stream2.stop_stream()
		self.stream2.close()
		self.p.terminate()

	def update(self,i):
		message1 = self.socket1.recv()
		new_message1 = message1.split()
		p1 = new_message1[1]
		q1 = new_message1[2]
		point1 = self.ax.scatter(q1,p1,color='green')
		point1.set_offsets(np.column_stack((q1,p1)))
		message2 = self.socket2.recv()
		new_message2 = message2.split()
		p2 = new_message2[1]
		q2 = new_message2[2]
		point2 = self.ax.scatter(q2,p2,color='red')
		point2.set_offsets(np.column_stack((q2,p2)))
		if (i+1)%50 ==0:
			if int(p1) > int(p2):
				self.stream1.write(self.data1)
				self.data1 = self.wf1.readframes(self.CHUNK1)
				# os.startfile('laugh.wav')
				
			else:
				self.stream2.write(self.data2)
				self.data2 = self.wf2.readframes(self.CHUNK2)
				# os.startfile('fanfare.wav')
				
		return point1,point2,
if __name__ == "__main__":	
	a = anim()
import zmq
import random
import sys
import time
import re
class Broad_Sub:
	def __init__(self,file_name):
		self.file_name = file_name
		self.sentences = []
		self.words = []
		self.pronouns = {}

	def analysis_file(self):
		pron = ['I','me',"We","we","us","you","You","he","she","It",'it']
		sentence_end_finder=re.compile(r"[\.\?!]")
		self.pronouns = {}
		message_data = ''
		new_word =''
		with open(self.file_name) as file_to_analysis:
			for line in file_to_analysis:
				sentence_words = 0
				for word in line.split():
					sentence_words += 1
					message_data=message_data+" "+word
					if sentence_end_finder.findall(word):
						print "%s" % (message_data)
						message_data = ''
						self.sentences.append(sentence_words)
						sentence_words = 0
						new_word = word[0:-1]
						self.words.append(len(new_word))
						if (new_word in pron) & (new_word in self.pronouns):
							self.pronouns[new_word] += 1
				        if (new_word in pron) & (new_word not in self.pronouns):
			        		self.pronouns[new_word] = 1
					else:
						self.words.append(len(word))
						if (word in pron) & (word in self.pronouns):
							self.pronouns[word] += 1
				        if (word in pron) & (word not in self.pronouns):
			        		self.pronouns[word] = 1

		self.sentences_avg = sum(self.sentences)/len(self.sentences)
		self.sentences_max = max(self.sentences)
		self.sentences_min = min(self.sentences)
		self.words_avg = sum(self.words)/len(self.words)
		self.words_max = max(self.words)
		self.words_min = min(self.words)
		freq = 0
		self.pronouns_max = {}
		pronouns_max = ''
		for key in self.pronouns:
			if self.pronouns[key] > freq:
				pronouns_max = key
				freq = self.pronouns[key]
		self.pronouns_max[pronouns_max] = freq

	def get_average_sentence(self):
		return self.sentences_avg

	def get_max_sentence(self):
		return self.sentences_max

	def get_min_sentence(self):
		return self.sentences_min
		

	def get_average_word(self):
		return self.words_avg

	def get_max_word(self):
		return self.words_max

	def get_min_word(self):
		return self.words_min

	def get_pronouns(self):
		return self.pronouns_max



class broadcaster(Broad_Sub):
	def __init__(self,port,file_name,delimiter=r"[\.\?!]",topic = 42):
		Broad_Sub.__init__(self,file_name)
		self.port = port
		context = zmq.Context()
		self.socket = context.socket(zmq.PUB)
		self.socket.bind("tcp://*:%s" % self.port)
		self.topic = topic
		self.delimiter = delimiter
		self.read_file()

	def read_file(self):       #fileName="fake_post_modernist_scholarship.txt"
		sentence_end_finder=re.compile(self.delimiter)    #r"[\.\?!]"
		message_data=""
		with open(self.file_name) as myFile:
		    for line in myFile:
		        for word in line.split():
		            message_data=message_data+" "+word
		            # print message data when a sentence ends
		            if sentence_end_finder.findall(word):
		                # print "from broadcaster %d %s" % (self.topic, message_data)
		                self.socket.send("%d %s" % (self.topic, message_data))
		                message_data=""
		                time.sleep(1)
if __name__ == "__main__":
	a = broadcaster("5556","HW5-Speech2.txt",r"[\.\?!]")
# a.analysis_file()
# print a.get_average_sentence()
# print a.get_average_word()
# print a.get_pronouns()
# b = Broad_Sub("fake_post_modernist_scholarship.txt")
# b.analysis_file()
# print b.get_average_sentence()
# print b.get_average_word()
# print b.get_pronouns()
# print b.pronouns
# class GeneralReactor():

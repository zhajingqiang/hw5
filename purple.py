import sys
import zmq
import time
import re

def purple(port = 5556,topic = 42,another_port1 = 5557,another_port2 = 5559):
	# if len(sys.argv) > 1:
	#     port =  sys.argv[1]
	#     int(port)
	    
	# if len(sys.argv) > 2:
	#     port1 =  sys.argv[2]
	#     int(port1)

	# Socket to talk to server
	context = zmq.Context()
	socket1 = context.socket(zmq.SUB)

	socket1.connect ("tcp://146.6.46.14:%s" % port)
	socket1.setsockopt(zmq.SUBSCRIBE, "%d"%topic)
	# if len(sys.argv) > 2:
	#     socket.connect ("tcp://localhost:%s" % port1)

	socket2 = context.socket(zmq.PUB)
	socket2.bind("tcp://*:%s" % another_port1)

	socket3 = context.socket(zmq.PUB)
	socket3.bind("tcp://*:%s" % another_port2)
	# print socket.recv.__doc__
	# poller1 = zmq.Poller()
	# poller1.register(socket1,zmq.POLLIN)
	# poller1.register(socket2,zmq.POLLIN)
	# poller1.register(socket3,zmq.POLLIN)

	short_sentences = 0
	short_words = 0
	first_person_pron = 0
	sentence_length = 0
	short_total = [0]
	s = len(str(topic))

	while True:
		try:
			# string = socket.recv(zmq.NOBLOCK)
			#print s
			# socks = dict(poller1.poll())
			# if socket1 in socks and socks[socket1] == zmq.POLLIN:
			string = socket1.recv()
			new_string = string[s:]
			long_word_database = ['yous guys', 'c++', 'shrubbery', 'star trek']
			short_word_database = [r"y'all",'python', 'hedge', 'star wars']
			# print string
			sentence_end_finder = re.compile(r"[\.\?!]")
			first_pron = ['He','he','Him','him','She','she','Her','her','It','it','They','they','Them','them']
			for word in new_string.split():
				sentence_length += 1
				if word in first_pron:
					first_person_pron += 1
				for element in short_word_database:
					if element in word:
						print 'Purple true',word
						short_words += 20
				if (word not in long_word_database) & (len(word) <= 6):
					short_words += 0
				if sentence_end_finder.findall(word): 
					if sentence_length<=10:
						short_sentences += 1
					sentence_length = 0
				short_total.append(short_sentences + short_words + first_person_pron)
				# print 'short from purple',short_sentences,short_words,first_person_pron, short_total
					# ani = animation.FuncAnimation(self.fig, self.update, interval=10,blit=False)
					# plt.show()
		# if socket2 in socks and socks[socket2] == zmq.POLLIN:
				socket2.send('%d %d %d'%(topic, short_total[-1],len(short_total)))
			# socket2.send ('%d %d'%(topic,short_total))
			# if message1 == "Exit":
			# 	print "Recieved exit command, client will stop recieving messages"
			# 	should_continue = False
		# if socket3 in socks and socks[socket3] == zmq.POLLIN:
				# socket3.send('%d %d %d'%(topic, short_total[-1],len(short_total)))

		except zmq.ZMQError:
			print "..."
			time.sleep(0.2)
if __name__ == "__main__":
	purple(5556,42)

import sys
import zmq
import time
import re
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
import numpy as np
import pyaudio
import wave

class green():
	def __init__(self,port=5556,topic = 42,another_port1 = 5559, another_port2 = 5558):
		self.port = port
		self.topic = topic
		self.another_port1 = another_port1
		self.another_port2 = another_port2
		self.long_total = [0]
		self.fig ,self.ax= plt.subplots()
		self.start()
	# if len(sys.argv) > 1:
	#     port =  sys.argv[1]
	#     int(port)
	    
	# if len(sys.argv) > 2:
	#     port1 =  sys.argv[2]
	#     int(port1)

	# Socket to talk to server
	def start(self):
		context = zmq.Context()
		socket1 = context.socket(zmq.SUB)
		socket1.connect ("tcp://146.6.46.14:%s" % self.port)
		socket1.setsockopt(zmq.SUBSCRIBE, "%d"%(self.topic))

		socket2 = context.socket(zmq.SUB)
		socket2.connect("tcp://localhost:%s" % self.another_port1)
		socket2.setsockopt(zmq.SUBSCRIBE, "%d"%(self.topic))

		socket3 = context.socket(zmq.PUB)
		socket3.bind("tcp://*:%s" % self.another_port2)
		# print socket.recv.__doc__
		# poller = zmq.Poller()
		# poller.register(socket1,zmq.POLLIN)
		# poller.register(socket2,zmq.POLLIN)
		# poller.register(socket3,zmq.POLLIN)

		long_sentences = 0
		long_words = 0
		third_person_pron = 0
		sentence_length = 0
		should_continue = True
		s = len(str(self.topic))
		while should_continue:
			try:
				# string = socket.recv(zmq.NOBLOCK)
				# socks = dict(poller.poll())
				# if socket1 in socks and socks[socket1] == zmq.POLLIN:
				string = socket1.recv()
				# print string
				messagedata = string[s:]
				long_word_database = ['yous guys', 'c++', 'shrubbery', 'star trek']
				short_word_database = [r"y'all", 'python', 'hedge', 'star wars']
				# print messagedata
				sentence_end_finder = re.compile(r"[\.\?!]")
				third_pron = ['He','he','Him','him','She','she','Her','her','It','it','They','they','Them','them']
				for word in messagedata.split():
					sentence_length += 1
					if word in third_pron:
						third_person_pron += 1
					for element in long_word_database:
						if element in word:
							print 'Green true',word
							long_words += 20
					if (word not in short_word_database) & (len(word) > 6):
						long_words += 1
					if sentence_end_finder.findall(word):
						if sentence_length>15:
							long_sentences += 1
						sentence_length = 0
					self.long_total.append(long_sentences + long_words + third_person_pron)
					# print 'long',long_sentences,long_words,third_person_pron,self.long_total[-1]

			# if socket2 in socks and socks[socket2] == zmq.POLLIN:
					# message2 = socket2.recv()
					# new_message2 = message2.split()
					# print new_message2,self.long_total[-1]
					# if new_message2[1] < self.long_total[-1]:
					# 	print 'Long words dominate'
					# else:
					# 	print 'Short words dominate'
				# print "Recieved control command: %s" % message2
				# if message1 == "Exit":
				# 	print "Recieved exit command, client will stop recieving messages"
				# 	should_continue = False
			# if socket3 in socks and socks[socket3] == zmq.POLLIN:
					socket3.send('%d %d %d'%(self.topic,self.long_total[-1],len(self.long_total)))   #
			except zmq.ZMQError:
				print "..."
				time.sleep(1)
	def update(self,_):
		p = range(0,len(self.long_total))
		point = self.ax.scatter(p,self.long_total)
		point.set_offsets(np.column_stack((p,self.long_total)))
		return point,

if __name__ == "__main__":
	a = green(5556,42,5557,5558)

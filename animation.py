import sys
import zmq
import time
import re
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
import numpy as np

def update(_):
	message1 = socket1.recv()
	new_message1 = message1.split()
	p1 = new_message1[1]
	q1 = new_message1[2]
	point1 = ax.scatter(q1,p1,color='green')
	point1.set_offsets(np.column_stack((q1,p1)))
	message2 = socket2.recv()
	new_message2 = message2.split()
	p2 = new_message2[1]
	q2 = new_message2[2]
	point2 = ax.scatter(q2,p2,color='red')
	point2.set_offsets(np.column_stack((q2,p2)))
	return point1,point2,
port1 = 5558
port2 = 5557
topic = 42
context = zmq.Context()
socket1 = context.socket(zmq.SUB)
socket1.connect ("tcp://localhost:%s" % port1)
socket1.setsockopt(zmq.SUBSCRIBE, "%d"% topic)
socket2 = context.socket(zmq.SUB)
socket2.connect ("tcp://localhost:%s" % port2)
socket2.setsockopt(zmq.SUBSCRIBE, "%d"% topic)
myfig,ax= plt.subplots()
ani = animation.FuncAnimation(myfig,update, interval=10,blit=False)
plt.show()					

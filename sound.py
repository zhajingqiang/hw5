import pyaudio
import wave
import sys

CHUNK = 1000


wf = wave.open('laugh.wav', 'rb')

p = pyaudio.PyAudio()

stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                channels=wf.getnchannels(),
                rate=wf.getframerate(),
                output=True)

data = wf.readframes(CHUNK)


stream.write(data)
data = wf.readframes(CHUNK)

stream.stop_stream()
stream.close()

p.terminate()